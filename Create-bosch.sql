
CREATE SCHEMA IF NOT EXISTS `bosch2` DEFAULT CHARACTER SET utf8 ;
USE `bosch2` ;
CREATE TABLE IF NOT EXISTS `bosch2`.`Client` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Surname` VARCHAR(25) NOT NULL,
  `Name` VARCHAR(25) NOT NULL,
  `Middle_name` VARCHAR(25) NOT NULL,
  `Country` VARCHAR(15) NOT NULL,
  `City` VARCHAR(15) NOT NULL,
  `Street` VARCHAR(15) NOT NULL,
  `House_number` VARCHAR(6) NOT NULL,
  `Phone` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bosch2`.`Worker`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bosch2`.`Worker` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Surname` VARCHAR(25) NOT NULL,
  `Name` VARCHAR(25) NOT NULL,
  `Middle_name` VARCHAR(25) NOT NULL,
  `City` VARCHAR(15) NOT NULL,
  `Street` VARCHAR(25) NOT NULL,
  `House_number` VARCHAR(6) NOT NULL,
  `Phone` VARCHAR(20) NOT NULL,
  `Position` VARCHAR(20) NOT NULL,
  `Start_date` DATE NOT NULL,
  `Hour_of_work` VARCHAR(45) NOT NULL,
  `Salary` DECIMAL NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bosch2`.`Producer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bosch2`.`Producer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NOT NULL,
  `City` VARCHAR(25) NOT NULL,
  `Street` VARCHAR(25) NOT NULL,
  `House_number` VARCHAR(45) NOT NULL,
  `Phone` VARCHAR(20) NOT NULL,
  `Email` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bosch2`.`Equipment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bosch2`.`Equipment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(45) NOT NULL,
  `Producer_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Equipment_Producer1_idx` (`Producer_id` ASC) VISIBLE,
  CONSTRAINT `fk_Equipment_Producer1`
    FOREIGN KEY (`Producer_id`)
    REFERENCES `bosch2`.`Producer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bosch2`.`Order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bosch2`.`Order` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Name_equipment` VARCHAR(30) NOT NULL,
  `Start_date` DATE NOT NULL,
  `End_date` DATE NOT NULL,
  `Cash` DECIMAL NOT NULL,
  `Worker_id` INT NOT NULL,
  `Status` VARCHAR(20) NOT NULL,
  `Worker_id1` INT NOT NULL,
  `Equipment_id` INT NOT NULL,
  `Client_id` INT NOT NULL,
  `Model` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Order_Worker1_idx` (`Worker_id1` ASC) VISIBLE,
  INDEX `fk_Order_Equipment1_idx` (`Equipment_id` ASC) VISIBLE,
  INDEX `fk_Order_Client1_idx` (`Client_id` ASC) VISIBLE,
  CONSTRAINT `fk_Order_Worker1`
    FOREIGN KEY (`Worker_id1`)
    REFERENCES `bosch2`.`Worker` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Order_Equipment1`
    FOREIGN KEY (`Equipment_id`)
    REFERENCES `bosch2`.`Equipment` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Order_Client1`
    FOREIGN KEY (`Client_id`)
    REFERENCES `bosch2`.`Client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bosch2`.`Type_of_work`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bosch2`.`Type_of_work` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Type_of_equipment` VARCHAR(45) NOT NULL,
  `Price` DECIMAL NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bosch2`.`Part`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bosch2`.`Part` (
  `Name` VARCHAR(45) NOT NULL,
  `id_order` INT NOT NULL,
  `Price` DECIMAL NOT NULL,
  `Status` VARCHAR(20) NOT NULL,
  `Producer_id` INT NOT NULL,
  PRIMARY KEY (`Name`),
  INDEX `fk_Part_Producer1_idx` (`Producer_id` ASC) VISIBLE,
  CONSTRAINT `fk_Part_Producer1`
    FOREIGN KEY (`Producer_id`)
    REFERENCES `bosch2`.`Producer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bosch2`.`Type_of_work_has_Order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bosch2`.`Type_of_work_has_Order` (
  `Type_of_work_id` INT NOT NULL,
  `Order_id` INT NOT NULL,
  PRIMARY KEY (`Type_of_work_id`, `Order_id`),
  INDEX `fk_Type_of_work_has_Order_Order1_idx` (`Order_id` ASC) VISIBLE,
  INDEX `fk_Type_of_work_has_Order_Type_of_work1_idx` (`Type_of_work_id` ASC) VISIBLE,
  CONSTRAINT `fk_Type_of_work_has_Order_Type_of_work1`
    FOREIGN KEY (`Type_of_work_id`)
    REFERENCES `bosch2`.`Type_of_work` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Type_of_work_has_Order_Order1`
    FOREIGN KEY (`Order_id`)
    REFERENCES `bosch2`.`Order` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bosch2`.`Order_has_Part`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bosch2`.`Order_has_Part` (
  `Order_id` INT NOT NULL,
  `Part_Name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Order_id`, `Part_Name`),
  INDEX `fk_Order_has_Part_Part1_idx` (`Part_Name` ASC) VISIBLE,
  INDEX `fk_Order_has_Part_Order1_idx` (`Order_id` ASC) VISIBLE,
  CONSTRAINT `fk_Order_has_Part_Order1`
    FOREIGN KEY (`Order_id`)
    REFERENCES `bosch2`.`Order` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Order_has_Part_Part1`
    FOREIGN KEY (`Part_Name`)
    REFERENCES `bosch2`.`Part` (`Name`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

