USE StoredPro_DB; 

DELIMITER //
CREATE TRIGGER Insert_employee
BEFORE INSERT 
ON employee for each row
BEGIN
	IF (NOT EXISTS(Select post From post Where post=new.post)) 
    THEN SIGNAL SQLSTATE'45000' SET MESSAGE_TEXT='There is no post in post table';
    END IF;
    IF (NOT EXISTS(Select id From pharmacy Where id=new.pharmacy_id)) 
    THEN SIGNAL SQLSTATE'45000' SET MESSAGE_TEXT='There is no pharmacy in pharmacy table';
    END IF;       
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER Update_employee
BEFORE update 
ON employee for each row
BEGIN
	IF (NOT EXISTS(Select post From post Where post=new.post)) 
    THEN SIGNAL SQLSTATE'45000' SET MESSAGE_TEXT='There is no post in post table';
    END IF;
    IF (NOT EXISTS(Select id From pharmacy Where id=new.pharmacy_id)) 
    THEN SIGNAL SQLSTATE'45000' SET MESSAGE_TEXT='There is no pharmacy in pharmacy table';
    END IF;       
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER Delete_post
BEFORE delete 
ON post for each row
BEGIN
	IF (EXISTS(Select post From employee Where post=old.post)) 
    THEN SIGNAL SQLSTATE'45000' SET MESSAGE_TEXT='There is this post in employee table';
    END IF;          
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER Delete_pharmacy
BEFORE delete 
ON pharmacy for each row
BEGIN
	IF (EXISTS(Select pharmacy_id From employee Where pharmacy_id=old.id)) 
    THEN SIGNAL SQLSTATE'45000' SET MESSAGE_TEXT='There is this pharmacy_id in employee table';
    END IF;          
END //
DELIMITER ;





DELIMITER //
CREATE TRIGGER Insert_pharmacy
BEFORE INSERT 
ON pharmacy for each row
BEGIN
	IF (NOT EXISTS(Select street From street Where street=new.street)) 
    THEN SIGNAL SQLSTATE'45000' SET MESSAGE_TEXT='There is no street in street table';
    END IF;        
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER Update_pharmacy
BEFORE update 
ON pharmacy for each row
BEGIN
	IF (NOT EXISTS(Select street From street Where street=new.street)) 
    THEN SIGNAL SQLSTATE'45000' SET MESSAGE_TEXT='There is no street in street table';
    END IF;        
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER Delete_street
BEFORE delete 
ON street for each row
BEGIN
	IF (EXISTS(Select street From pharmacy Where street=old.street)) 
    THEN SIGNAL SQLSTATE'45000' SET MESSAGE_TEXT='There is this street in pharmacy table';
    END IF;          
END //
DELIMITER ;







DELIMITER //
CREATE TRIGGER Insert_pharmacy_medicine
BEFORE INSERT 
ON pharmacy_medicine for each row
BEGIN
	IF (NOT EXISTS(Select id From pharmacy  Where id=new.pharmacy_id)) 
    THEN SIGNAL SQLSTATE'45000' SET MESSAGE_TEXT='There is no pharmacy_id in pharmacy table';
    END IF;        
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER Update_pharmacy_medicine
BEFORE update 
ON pharmacy_medicine for each row
BEGIN
	IF (NOT EXISTS(Select id From pharmacy Where id=new.pharmacy_id)) 
    THEN SIGNAL SQLSTATE'45000' SET MESSAGE_TEXT='There is no pharmacy_id in pharmacy table';
    END IF;        
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER Delete_pharmacy2
BEFORE delete 
ON pharmacy for each row
BEGIN
	IF (EXISTS(Select pharmacy_id From pharmacy_medicine Where pharmacy_id=old.id)) 
    THEN SIGNAL SQLSTATE'45000' SET MESSAGE_TEXT='There is this pharmacy_id in pharmacy table';
    END IF;          
END //
DELIMITER ;


DELIMITER //
CREATE TRIGGER Insert_pharmacy_medicine2
BEFORE INSERT 
ON pharmacy_medicine for each row
BEGIN
	IF (NOT EXISTS(Select id From medicine  Where id=new.medicine_id)) 
    THEN SIGNAL SQLSTATE'45000' SET MESSAGE_TEXT='There is no medicine_id in medicine table';
    END IF;        
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER Update_pharmacy_medicine2
BEFORE update 
ON pharmacy_medicine for each row
BEGIN
	IF (NOT EXISTS(Select id From medicine Where id=new.medicine_id)) 
    THEN SIGNAL SQLSTATE'45000' SET MESSAGE_TEXT='There is no medicine_id in medicine table';
    END IF;        
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER Delete_medicine
BEFORE delete 
ON medicine for each row
BEGIN
	IF (EXISTS(Select medicine_id From pharmacy_medicine Where medicine_id=old.id)) 
    THEN SIGNAL SQLSTATE'45000' SET MESSAGE_TEXT='There is this medicine_id in pharmacy_medicine table';
    END IF;   
    IF (EXISTS(Select medicine_id From medicine_zone Where medicine_id=old.id)) 
    THEN SIGNAL SQLSTATE'45000' SET MESSAGE_TEXT='There is this medicine_id in medicine_zone table';
    END IF; 
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER Insert_medicine_zone
BEFORE INSERT 
ON medicine_zone for each row
BEGIN
	IF (NOT EXISTS(Select id From medicine  Where id=new.medicine_id)) 
    THEN SIGNAL SQLSTATE'45000' SET MESSAGE_TEXT='There is no medicine_id in medicine table';
    END IF; 
    IF (NOT EXISTS(Select id From zone  Where id=new.medicine_id)) 
    THEN SIGNAL SQLSTATE'45000' SET MESSAGE_TEXT='There is no medicine_id in zone table';
    END IF; 
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER Update_medicine_zone
BEFORE update 
ON medicine_zone for each row
BEGIN
	IF (NOT EXISTS(Select id From medicine  Where id=new.medicine_id)) 
    THEN SIGNAL SQLSTATE'45000' SET MESSAGE_TEXT='There is no medicine_id in medicine table';
    END IF; 
    IF (NOT EXISTS(Select id From zone  Where id=new.medicine_id)) 
    THEN SIGNAL SQLSTATE'45000' SET MESSAGE_TEXT='There is no medicine_id in zone table';
    END IF;       
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER Delete_zone
BEFORE delete 
ON zone for each row
BEGIN
	IF (EXISTS(Select zone_id From medicine_zone Where zone_id=old.id)) 
    THEN SIGNAL SQLSTATE'45000' SET MESSAGE_TEXT='There is this zone_id in medicine_zone table';
    END IF;   
    
END //
DELIMITER ;




/*
DELIMITER //
create trigger BeforeInsertPharmacy
before insert 
on pharmacy for each row
begin
	declare inner_pharmacy_id int;
    select max(id)+1 into inner_pharmacy_id from pharmacy;
    if inner_pharmacy_id is null then set inner_pharmacy_id = 1;
    end if;
    if (not exists(select street from street where street=new.street)) 
    then signal sqlstate'45000' set message_text='there is no this element in table STREET';
    end if;

    set new.id = inner_pharmacy_id;
end //
DELIMITER ;



DELIMITER //
create trigger BeforeInsertMedicineZone
before insert
on  medicine_zone for each row
begin
	if(not exists(select id from medicine where id=medicine_id))
    then signal sqlstate'45000' set message_text='there is no this element in table MEDICINE';
    end if;
    if(not exists(select id from zone where id=zone_id))
    then signal sqlstate'45000' set message_text='there is no this element in table ZONE';
    end if;
end //
DELIMITER ;


DELIMITER //
create trigger BeforeInsertPharmacyMedicine
before insert
on  pharmacy_medicine for each row
begin
    if(not exists(select id from pharmacy where id=pharmacy_id))
    then signal sqlstate'45000' set message_text='there is no this element in table PHARMACY';
    end if;
    if(not exists(select id from medicine where id=medicine_id))
    then signal sqlstate'45000' set message_text='there is no this element in table MEDICINE';
    end if;
end //
DELIMITER ;


DELIMITER //
create trigger BeforeUpdateEmployee
before update
on employee for each row
begin
    if (not exists(select post from post where post=new.post)) 
    then signal sqlstate'45000' set message_text='there is no this element in table POST';
    end if;
    if (not exists(select id from pharmacy where id=new.pharmacy_id)) 
    then signal sqlstate'45000' set message_text='there is no this element in table PHARMACY';
    end if;
end //
DELIMITER ;


DELIMITER //
create trigger BeforeUpdatePharmacy
before update
on pharmacy for each row
begin
    if (not exists(select street from street where street=new.street)) 
    then signal sqlstate'45000' set message_text='there is no this element in table STREET';
    end if;
end //
DELIMITER ;


DELIMITER //
create trigger BeforeUpdateMedicineZone
before update
on  medicine_zone for each row
begin
	if(not exists(select id from medicine where id=medicine_id))
    then signal sqlstate'45000' set message_text='there is no this element in table MEDICINE';
    end if;
    if(not exists(select id from zone where id=zone_id))
    then signal sqlstate'45000' set message_text='there is no this element in table ZONE';
    end if;
end //
DELIMITER ;


DELIMITER //
create trigger BeforeUpdatePharmacyMedicine
before update
on  pharmacy_medicine for each row
begin
    if(not exists(select id from pharmacy where id=pharmacy_id))
    then signal sqlstate'45000' set message_text='there is no this element in table PHARMACY';
    end if;
    if(not exists(select id from medicine where id=medicine_id))
    then signal sqlstate'45000' set message_text='there is no this element in table MEDICINE';
    end if;
end //
DELIMITER ;
*/


