USE StoredPro_DB; 

DELIMITER //
CREATE TRIGGER Trigger_Insert_employee
BEFORE INSERT 
ON employee for each row
BEGIN
	IF (new.identity_number LIKE '%00')  
    THEN SIGNAL SQLSTATE '45000'    SET MESSAGE_TEXT = 'CHECK error for Insert';  
    END IF;      
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER Trigger_Insert_medicine
BEFORE INSERT 
ON medicine for each row
BEGIN
	IF (new.ministry_code NOT LIKE '[^MP][^MP]-[0-9][0-9][0-9]-[0-9][0-9]')  
    THEN SIGNAL SQLSTATE '45000'    SET MESSAGE_TEXT = 'CHECK error for Insert';  
    END IF;      
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER Trigger_Update_post
BEFORE UPDATE 
ON post for each row
BEGIN
	IF (new.post)  
    THEN SIGNAL SQLSTATE '45000'    SET MESSAGE_TEXT = 'CHECK error for Insert';  
    END IF;      
END //
DELIMITER ;

