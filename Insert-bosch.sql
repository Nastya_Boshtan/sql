Use bosch;
Insert into bosch.order (Name_equipment,Start_date, End_date, Cash, Status, Worker_id, Equipment_id, Client_id, Model)
-- Values ('Dishwasher',20191011,20191012,80, 'Repaired',2,3,7,'SMV26VX00T');
Values('Boiler',20191011,20191020,250, 'In repaire',2,7,6,'TRONIC8000TEN'),
('Oven',20191011,20191012,80, 'Repaired',3,5,9,'HBF114EB0R');

Insert into type_of_work_has_order ()
Values(2,1),
(5,2),
(3,3),
(5,4),
(10,5),
(8,6);
